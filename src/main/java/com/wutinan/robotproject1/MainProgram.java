/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wutinan.robotproject1;

import java.util.Scanner;

/**
 *
 * @author INGK
 */
public class MainProgram {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        TableMap map = new TableMap(15, 15);
        Robot robot = new Robot(2, 2, 'x', map, 10);
        Bomb bomb = new Bomb(5, 5);
        map.addObj(new Tree(10, 10));
        map.addObj(new Tree(5, 10));
        map.addObj(new Tree(9, 10));
        map.addObj(new Tree(11, 14));
        map.addObj(new Tree(10, 14));
        map.addObj(new Tree(9, 13));
        map.addObj(new Tree(9, 14));
        map.addObj(new Tree(3, 14));
        map.addObj(new Tree(6, 4));
        map.addObj(new Tree(7, 11));
        map.addObj(new Tree(3, 14));
        map.addObj(new Tree(9, 13));
        map.addObj(new Fuel(0, 5, 10));
        map.addObj(new Fuel(3, 7, 10));
        map.addObj(new Fuel(11, 9, 10));
        map.setBomb(bomb);
        map.setRobot(robot);
        while (true) {
            map.showMap();
            // W,a| N,w| E,d| S, s| q: quit
            char direction = inputDirection(sc);
            if (direction == 'q') {
                printByeBye();
                break;
            }
            robot.walk(direction);
        }

    }

    private static void printByeBye() {
        System.out.println("Bye Bye!!!");
    }

    private static char inputDirection(Scanner sc) {
        String str = sc.next();
        return str.charAt(0);
    }
}
